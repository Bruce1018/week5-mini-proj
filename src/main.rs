use lambda_runtime::{LambdaEvent, Error as LambdaError, service_fn};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use aws_config::load_from_env;
use aws_sdk_dynamodb::{Client, model::AttributeValue};
use std::collections::HashMap;

// Define a struct to represent the request payload received by the Lambda function
#[derive(Deserialize)]
struct Request {
    productname: String,
}

// Define a struct to represent the response payload to be returned by the Lambda function
#[derive(Serialize)]
struct Response {
    price: f64,
}

// Function to search for product price in DynamoDB based on the product name
async fn search_product_price(client: &Client, productname: String) -> Result<f64, LambdaError> {
    let table_name = "ProductsTable";
    let mut expr_attr_values = HashMap::new();
    expr_attr_values.insert(":productname_val".to_string(), AttributeValue::S(productname));

    let result = client.query()
        .table_name(table_name)
        .key_condition_expression("productname = :productname_val")
        .set_expression_attribute_values(Some(expr_attr_values)) // Corrected usage here
        .send()
        .await?;

    // Extract the first item from the query result as it should be unique by product name
    let item = result.items().and_then(|items| items.first());

    match item {
        Some(item) => {
            let price_attr = item.get("price").and_then(|val| val.as_n().ok());
            match price_attr {
                Some(price) => Ok(price.parse::<f64>().unwrap_or_default()),
                None => Err(LambdaError::from("No price found for the given product name")),
            }
        },
        None => Err(LambdaError::from("No product found with the given name")),
    }
}

async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    // Deserialize the incoming JSON payload into the Request struct
    let request: Request = serde_json::from_value(event.payload)?;

    let config = load_from_env().await;
    let client = Client::new(&config);

    // Search for the product price based on the product name
    let price = search_product_price(&client, request.productname).await?;

    // Serialize the response into JSON format and return it
    Ok(json!({
        "price": price,
    }))
}

// The main entry point for the Lambda function, utilizing the `tokio` runtime
#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    // Create a service function using the handler function and run the Lambda function
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}
