# Week5-mini-proj

> Zilin(Bruce) Xu  
> zx112

# Project Description and Demo
In my project, I design a simple `menu system`. In the database system, each attributs has two variables: **price** and **name**.
If you type the input product name, the system will return the price of that item as following picture.

![](image1.png)
![](image2.png)

## Step 1: Local Setup

0. Create a new project on `Gitlab` and clone on local.
1. Create a new `cargo project` by using the command `cargo lambda new <project-name>`.
2. Edit the `.toml` file and `main.rs` file to reach the requirement
3. Run some test cases

## Step 2: Upload on AWS
0. Run the command `cargo lambda build --release` and `cargo lambda deploy --region us-east-1 --iam-role arn:aws:iam::905418286131:role/LambdaExecutionRole` (as we did in week2). Then you should see it deploy on **AWS**.
1. Create table on the `DynamoDB`, add some items in it. Remeber to change the code from `main.rs` so that the table name is the same.
2. Clink `add trigger` on Lambda to create a new `Rest API` 

Here are some screen shots
![](image3.png)
![](image4.png)